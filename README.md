# Event Manager Map Fix Block Themes

Patches to allow event manager to display maps via shortcode and from the search bar when using a block theme

## Copy files


    cp event*.js <wordpress>/wp-content/plugins/events-manager/includes/js
    
    cp maps.js <wordpress>/wp-content/plugins/events-manager/includes/js/src/parts

    cp em-actions.php <wordpress>/wp-content/plugins/events-manager/

## Copy plugin folder

To directly add to your wordpress instance copy the plugin folder into your plugins folder, or zip up and install through the dashboard.

Once installed de-activate the official plugin and activate this plugin
